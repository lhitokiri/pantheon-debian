#!/bin/bash

if [ $# -lt 2 ]
then
    echo "$0 <branch> <package> [package release]"
    exit 1
fi

. $PD_ROOT_PATH/bin/functions

load_conf

BRANCH=$1
if [ $(branch_exist $BRANCH) = 0 ]
then
    echo_msg "ERROR" "Branch $BRANCH not found !!" $red
    unload_conf
    exit 1
fi

PACKAGE=$2
if [ $(package_section_name_exist $BRANCH $PACKAGE) = 0 ]
then
    echo_msg "ERROR" "Package $PACKAGE not found in $BRANCH !!" $red
    unload_conf
    exit 1
fi

PKG_RELEASE=1
if [ ! x"$3"  = x"" ]
then
    PKG_RELEASE=$3
fi

PKG_SECTION=$(get_package_section_from_name $BRANCH $PACKAGE)
PKG_PATH=$(get_package_section_path $BRANCH $PKG_SECTION)
if [ "x$PKG_PATH" = "x" ]
then
    echo_msg "ERROR" "$PACKAGE path not found" $red
    unload_conf
    exit 1
fi

PKG_GIT_REPO=$(get_package_section_git_repository $BRANCH $PKG_SECTION)
if [ "x$PKG_GIT_REPO" = "x" ]
then
    echo_msg "ERROR" "Package $PACKAGE does not have git repository !!" $red
    unload_conf
    exit 1
fi

if [ ! -e $PKG_PATH ]
then
    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Clone $PKG_GIT_REPO ... " $green
    git clone $PKG_GIT_REPO $PKG_PATH > /dev/null 2>&1 || push_error
    check_error
fi

trap_error
echo_msgn "CHECKOUT PACKAGE" "Open $PKG_PATH ... " $green
cd $PKG_PATH  > /dev/null 2>&1 || push_error
check_error

GIT_RECIPE_BRANCH=$(get_package_section_git_recipe_branch $BRANCH $PKG_SECTION)

LP_UPSTREAM_BRANCH=$(get_package_section_lp_branch $BRANCH $PKG_SECTION)
GIT_UPSTREAM_REPOSITORY=$(get_package_section_git_upstream_repository $BRANCH $PKG_SECTION)
GIT_UPSTREAM_BRANCH=$(get_package_section_git_upstream_branch $BRANCH $PKG_SECTION)
TARBALL_UPSTREAM=$(get_package_section_upstream_tarball $BRANCH $PKG_SECTION)
USCAN_UPSTREAM=$(get_package_section_uscan $BRANCH $PKG_SECTION)

if [ ! "x$LP_UPSTREAM_BRANCH" = "x" ]
then
    echo_msgn "CHECKOUT PACKAGE" "Delete all old upstream tags ... " $green
    for i in $(git tag | grep -e "^upstream-$(get_branch_name ${BRANCH})/" -e "^$(get_branch_name ${BRANCH})/" 2> /dev/null)
    do
        git tag -d $i > /dev/null 2>&1 || push_error;
    done
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Add upstream launchpad branch $LP_UPSTREAM_BRANCH ... " $green
    git remote add $(get_branch_name $BRANCH) "bzr::$LP_UPSTREAM_BRANCH" > /dev/null 2>&1
    if [ ! $? = "0" ]
    then
        git remote set-url $(get_branch_name $BRANCH) "bzr::$LP_UPSTREAM_BRANCH" > /dev/null 2>&1 || push_error
    fi
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Fetch upstream launchpad branch $LP_UPSTREAM_BRANCH ... " $green
    git fetch $(get_branch_name $BRANCH) > /dev/null 2>&1 || push_error
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Clone upstream launchpad branch $LP_UPSTREAM_BRANCH in git $GIT_UPSTREAM_BRANCH ... " $green
    git checkout -b $GIT_UPSTREAM_BRANCH $(get_branch_name $BRANCH)/master > /dev/null 2>&1
    if [ ! $? = "0" ]
    then
        git checkout $GIT_RECIPE_BRANCH > /dev/null 2>&1 || push_error
        git branch -D $GIT_UPSTREAM_BRANCH > /dev/null 2>&1 || push_error
        git checkout -b $GIT_UPSTREAM_BRANCH $(get_branch_name $BRANCH)/master > /dev/null 2>&1 || push_error
    fi
    check_error

    GIT_UPSTREAM_REPOSITORY_TAG=$(get_package_section_git_upstream_repository_tag $BRANCH $PKG_SECTION)
    if [ ! "x$GIT_UPSTREAM_REPOSITORY_TAG" = "x" ]
    then
        trap_error
        echo_msgn "CHECKOUT PACKAGE" "Checkout upstream tags $GIT_UPSTREAM_REPOSITORY_TAG in git $GIT_UPSTREAM_BRANCH ... " $green
        git fetch $(get_branch_name $BRANCH) --tags > /dev/null 2>&1 || push_error
        git reset --hard $GIT_UPSTREAM_REPOSITORY_TAG  > /dev/null 2>&1 || push_error
        check_error
    fi
elif [ ! "x$GIT_UPSTREAM_REPOSITORY" = "x" ]
then
    GIT_UPSTREAM_REPOSITORY_BRANCH=$(get_package_section_git_upstream_repository_branch $BRANCH $PKG_SECTION)
    GIT_UPSTREAM_REPOSITORY_TAG=$(get_package_section_git_upstream_repository_tag $BRANCH $PKG_SECTION)

    echo_msgn "CHECKOUT PACKAGE" "Delete all old upstream tags ... " $green
    for i in $(git tag | grep -e "^upstream-$(get_branch_name ${BRANCH})/" -e "^$(get_branch_name ${BRANCH})/" 2> /dev/null)
    do
        git tag -d $i > /dev/null 2>&1 || push_error;
    done
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Add git upstream repository $GIT_UPSTREAM_REPOSITORY ... " $green
    git remote add $(get_branch_name $BRANCH) "$GIT_UPSTREAM_REPOSITORY" > /dev/null 2>&1
    if [ ! $? = "0" ]
    then
        git remote set-url $(get_branch_name $BRANCH) "$GIT_UPSTREAM_REPOSITORY" > /dev/null 2>&1 || push_error
    fi
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Fetch git upstream repository $GIT_UPSTREAM_REPOSITORY ... " $green
    git fetch $(get_branch_name $BRANCH) > /dev/null 2>&1 || push_error
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Clone upstream repository $GIT_UPSTREAM_REPOSITORY in git $GIT_UPSTREAM_BRANCH ... " $green
    git checkout -b $GIT_UPSTREAM_BRANCH $(get_branch_name $BRANCH)/$GIT_UPSTREAM_REPOSITORY_BRANCH > /dev/null 2>&1
    if [ ! $? = "0" ]
    then
        git checkout $GIT_RECIPE_BRANCH > /dev/null 2>&1 || push_error
        git branch -D $GIT_UPSTREAM_BRANCH > /dev/null 2>&1 || push_error
        git checkout -b $GIT_UPSTREAM_BRANCH $(get_branch_name $BRANCH)/$GIT_UPSTREAM_REPOSITORY_BRANCH > /dev/null 2>&1 || push_error
    fi
    check_error

    if [ ! "x$GIT_UPSTREAM_REPOSITORY_TAG" = "x" ]
    then
        trap_error
        echo_msgn "CHECKOUT PACKAGE" "Checkout upstream tags $GIT_UPSTREAM_REPOSITORY_TAG in git $GIT_UPSTREAM_BRANCH ... " $green
        git fetch $(get_branch_name $BRANCH) --tags > /dev/null 2>&1 || push_error
        git reset --hard $GIT_UPSTREAM_REPOSITORY_TAG  > /dev/null 2>&1 || push_error
        check_error
    fi
elif [ ! "x$TARBALL_UPSTREAM" = "x" ]
then
    echo_msgn "CHECKOUT PACKAGE" "Delete all old upstream tags ... " $green
    for i in $(git tag | grep -e "^upstream-$(get_branch_name ${BRANCH})/" -e "^$(get_branch_name ${BRANCH})/" 2> /dev/null)
    do
        git tag -d $i > /dev/null 2>&1 || push_error;
    done
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Create upstream branch $GIT_UPSTREAM_BRANCH ... " $green
    git checkout --orphan $GIT_UPSTREAM_BRANCH > /dev/null 2>&1
    if [ ! $? = "0" ]
    then
        git checkout $GIT_RECIPE_BRANCH > /dev/null 2>&1 || push_error
        git branch -D $GIT_UPSTREAM_BRANCH > /dev/null 2>&1 || push_error
        git checkout --orphan $GIT_UPSTREAM_BRANCH > /dev/null 2>&1 || push_error
    fi
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Init upstream branch $GIT_UPSTREAM_BRANCH ... " $green
    git rm -rf .  > /dev/null 2>&1 || push_error
    git commit --allow-empty -m "Initial commit" > /dev/null 2>&1 || push_error
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Download upstream tarball $TARBALL_UPSTREAM ... " $green
    rm -f ../$(basename $TARBALL_UPSTREAM) > /dev/null 2>&1
    curl -L $TARBALL_UPSTREAM > ../$(basename $TARBALL_UPSTREAM) 2> /dev/null || push_error
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Import upstream tarball $(basename $TARBALL_UPSTREAM) in $GIT_UPSTREAM_BRANCH ... " $green
    gbp import-orig --no-merge --no-interactive --upstream-branch=$GIT_UPSTREAM_BRANCH --upstream-tag="$(get_branch_name ${BRANCH})/%(version)s" ../$(basename $TARBALL_UPSTREAM)  > /dev/null 2>&1 || push_error
    rm -f ../$(basename $TARBALL_UPSTREAM) > /dev/null 2>&1 || push_error
    check_error
elif [ ! "x$USCAN_UPSTREAM" = "x" ]
then
    echo_msgn "CHECKOUT PACKAGE" "Delete all old upstream tags ... " $green
    for i in $(git tag | grep -e "^upstream-$(get_branch_name ${BRANCH})/" -e "^$(get_branch_name ${BRANCH})/" 2> /dev/null)
    do
        git tag -d $i > /dev/null 2>&1 || push_error;
    done
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Create upstream branch $GIT_UPSTREAM_BRANCH ... " $green
    git checkout --orphan $GIT_UPSTREAM_BRANCH > /dev/null 2>&1
    if [ ! $? = "0" ]
    then
        git checkout $GIT_RECIPE_BRANCH > /dev/null 2>&1 || push_error
        git branch -D $GIT_UPSTREAM_BRANCH > /dev/null 2>&1 || push_error
        git checkout --orphan $GIT_UPSTREAM_BRANCH > /dev/null 2>&1 || push_error
    fi
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Init upstream branch $GIT_UPSTREAM_BRANCH ... " $green
    git rm -rf .  > /dev/null 2>&1 || push_error
    git commit --allow-empty -m "Initial commit" > /dev/null 2>&1 || push_error
    check_error

    echo_msgn "CHECKOUT PACKAGE" "Watch for upstream tarball ... " $green
    echo "version=3" > ../$PACKAGE.watch
    echo "$USCAN_UPSTREAM" >> ../$PACKAGE.watch
    TARBALL_UPSTREAM=$(uscan -dd --watchfile ../$PACKAGE.watch --package $PACKAGE --upstream-version 0.0.0 --dehs 2> /dev/null | grep target-path | sed -e 's^<[/]*target-path>^^g')
    if [ ! "x$TARBALL_UPSTREAM" = "x" ]
    then
        echoc "OK" $green
        rm -f ../$PACKAGE.watch
    else
        echoc "ERROR" $red;
        rm -f ../$PACKAGE.watch
        unload_conf
        exit 1
    fi

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Import upstream tarball $(basename $TARBALL_UPSTREAM) in $GIT_UPSTREAM_BRANCH ... " $green
    gbp import-orig --no-merge --no-interactive --upstream-branch=$GIT_UPSTREAM_BRANCH --upstream-tag="$(get_branch_name ${BRANCH})/%(version)s" ../$(basename $TARBALL_UPSTREAM)  > /dev/null 2>&1 || push_error
    rm -f ../$(basename $TARBALL_UPSTREAM) > /dev/null 2>&1 || push_error
    rm -f ../$(basename $TARBALL_UPSTREAM | sed -e 's/\.orig//' | sed -e 's/_/-/') > /dev/null 2>&1 || push_error
    check_error
else
    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Checkout upstream branch $GIT_UPSTREAM_BRANCH ... " $green
    git checkout $GIT_UPSTREAM_BRANCH > /dev/null 2>&1 || push_error
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Update upstream branch $GIT_RECIPE_BRANCH ... " $green
    git pull --rebase > /dev/null 2>&1 || push_error
    check_error
fi

LP_RECIPE_BRANCH=$(get_package_section_recipe_lp_branch $BRANCH $PKG_SECTION)

if [ ! "x$LP_RECIPE_BRANCH" = "x" ]
then
    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Add recipe launchpad branch $LP_RECIPE_BRANCH ... " $green
    git remote add pantheon-debian-$(get_branch_name $BRANCH) "bzr::$LP_RECIPE_BRANCH" > /dev/null 2>&1
    if [ ! $? = "0" ]
    then
        git remote set_url pantheon-debian-$(get_branch_name $BRANCH) "bzr::$LP_RECIPE_BRANCH" > /dev/null 2>&1 || push_error
    fi
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Fetch recipe launchpad branch $LP_RECIPE_BRANCH ... " $green
    git fetch pantheon-debian-$(get_branch_name $BRANCH) > /dev/null 2>&1 || push_error
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Clone recipe launchpad branch $LP_RECIPE_BRANCH in git $GIT_RECIPE_BRANCH ... " $green
    git checkout -b $GIT_RECIPE_BRANCH pantheon-debian-$(get_branch_name $BRANCH)/master > /dev/null 2>&1 || push_error
    check_error
else
    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Checkout recipe branch $GIT_RECIPE_BRANCH ... " $green
    git checkout $GIT_RECIPE_BRANCH > /dev/null 2>&1
    if [ ! $? = "0" ]
    then
        git checkout origin/$GIT_RECIPE_BRANCH -b $GIT_RECIPE_BRANCH > /dev/null 2>&1 || push_error
    fi
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Update recipe branch $GIT_RECIPE_BRANCH ... " $green
    git pull --rebase > /dev/null 2>&1 || git pull --rebase > /dev/null 2>&1 || push_error
    check_error
fi

GIT_BRANCH=$(get_package_section_git_branch $BRANCH $PKG_SECTION)
trap_error
echo_msgn "CHECKOUT PACKAGE" "Checkout upstream branch $GIT_UPSTREAM_BRANCH ... " $green
git checkout $GIT_UPSTREAM_BRANCH > /dev/null 2>&1 || push_error
check_error

trap_error
echo_msgn "CHECKOUT PACKAGE" "Clone upstream branch $GIT_UPSTREAM_BRANCH in $GIT_BRANCH ... " $green
git checkout -b $GIT_BRANCH > /dev/null 2>&1
if [ ! $? = "0" ]
then
    git branch -D $GIT_BRANCH > /dev/null 2>&1
    git checkout -b $GIT_BRANCH > /dev/null 2>&1 || push_error
fi
check_error

trap_error
echo_msgn "CHECKOUT PACKAGE" "Merge branch $GIT_RECIPE_BRANCH in $GIT_BRANCH ... " $green
git merge -X theirs --no-edit --allow-unrelated-histories $GIT_RECIPE_BRANCH > /dev/null 2>&1 || push_error
check_error

VERSION=$(get_last_package_version)
FULL_VERSION=$(get_last_package_version epoch)
BZR_REV=$(get_last_bzr_commit $BRANCH $PKG_SECTION)
GIT_UPSTREAM_REV=$(git rev-list --count $GIT_UPSTREAM_BRANCH)
if [ ! "x$USCAN_UPSTREAM" = "x" ]
then
    SNAP_VERSION=${VERSION}
    EPOCH_VERSION=${FULL_VERSION}
elif [ "$(get_package_section_create_snapshot $BRANCH $PKG_SECTION)" = "1" ]
then
    SNAP_VERSION=${VERSION}${BZR_REV:++bzr$BZR_REV}${GIT_UPSTREAM_REPOSITORY:++git$GIT_UPSTREAM_REV}
    EPOCH_VERSION=${FULL_VERSION}${BZR_REV:++bzr$BZR_REV}${GIT_UPSTREAM_REPOSITORY:++git$GIT_UPSTREAM_REV}
else
    SNAP_VERSION=${VERSION}
    EPOCH_VERSION=${FULL_VERSION}
fi
UPSTREAM_TAG="upstream-$(get_branch_name ${BRANCH})/$SNAP_VERSION"
if [ "x$(git tag | grep $UPSTREAM_TAG)" = "x" ]
then
    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Checkout upstream branch $GIT_UPSTREAM_BRANCH ... " $green
    git checkout $GIT_UPSTREAM_BRANCH > /dev/null 2>&1 || push_error
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Tag upstream branch with $UPSTREAM_TAG ... " $green
    git tag $UPSTREAM_TAG > /dev/null 2>&1 || push_error
    check_error

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Checkout build branch $GIT_BRANCH ... " $green
    git checkout $GIT_BRANCH > /dev/null 2>&1 || push_error
    check_error
fi

if [ ! "x$USCAN_UPSTREAM" = "x" ]
then
    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Create entry in debian/changelog ... " $green
    debchange --force-save-on-release -v "$EPOCH_VERSION-0+pantheon+$(get_branch_version $BRANCH)$PKG_RELEASE" -D unstable "New upstream release." > /dev/null 2>&1 || push_error
    check_error

    if [ -e .gitattributes ]
    then
        git rm .gitattributes > /dev/null 2>&1
    fi

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Commit changes ... " $green
    VERSION=$(dpkg-parsechangelog | grep "Version:" | awk '{print $2}')
    CHANGES=$(dpkg-parsechangelog | awk 'BEGIN {show=0;} {if (show == 1) print $0; else if ($0 == " .") show=1;}')
    printf "Release %s:\n%s\n" "$VERSION" "$CHANGES" | git commit -a -F - > /dev/null 2>&1 || push_error
    check_error
elif [ "$(get_package_section_create_snapshot $BRANCH $PKG_SECTION)" = "1" ]
then
    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Create entry in debian/changelog ... " $green
    debchange --force-save-on-release -v "$EPOCH_VERSION-0+pantheon+$(get_branch_version $BRANCH)$PKG_RELEASE" -D unstable "New snapshot release." > /dev/null 2>&1 || push_error
    check_error

    if [ -e .gitattributes ]
    then
        git rm .gitattributes > /dev/null 2>&1
    fi

    trap_error
    echo_msgn "CHECKOUT PACKAGE" "Commit changes ... " $green
    VERSION=$(dpkg-parsechangelog | grep "Version:" | awk '{print $2}')
    CHANGES=$(dpkg-parsechangelog | awk 'BEGIN {show=0;} {if (show == 1) print $0; else if ($0 == " .") show=1;}')
    printf "Release %s:\n%s\n" "$VERSION" "$CHANGES" | git commit -a -F - > /dev/null 2>&1 || push_error
    check_error
fi

unload_conf

exit 0

