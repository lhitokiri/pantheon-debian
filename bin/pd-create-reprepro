#!/bin/bash

if [ $# -lt 1 ]
then
    echo "$0 <branch>"
    exit 1
fi

. $PD_ROOT_PATH/bin/functions

load_conf

BRANCH=$1
if [ $(branch_exist $BRANCH) = 0 ]
then
    echo_msg "ERROR" "Branch $BRANCH not found !!" $red
    unload_conf
    exit 1
fi

REPREPRO_PATH=$(get_reprepro_path)
if [ ! -e $REPREPRO_PATH ]
then
    trap_error
    echo_msgn "CREATE REPREPRO" "Create reprepro path $REPREPRO_PATH ... " $green
    mkdir -p $REPREPRO_PATH > /dev/null 2>&1 || push_error
    check_error
fi

if [ ! -e $REPREPRO_PATH/conf ]
then
    trap_error
    echo_msgn "CREATE REPREPRO" "Create reprepro path $REPREPRO_PATH/conf ... " $green
    mkdir -p $REPREPRO_PATH/conf > /dev/null 2>&1 || push_error
    check_error
fi

if [ -e $REPREPRO_PATH/conf/distributions ]
then
    REPOS_EXISTS=$(grep "Origin: pantheon-debian-$BRANCH" $REPREPRO_PATH/conf/distributions 2> /dev/null)
    if [ ! "x$REPOS_EXISTS" = "x" ]
    then
        echo_msg "ERROR" "Repository pantheon-debian-$BRANCH already exists !!" $red
        unload_conf
        exit 1
    fi
fi

trap_error
echo_msgn "CREATE REPREPRO" "Create reprepro $BRANCH configuration ... " $green
echo "Origin: pantheon-debian-$(get_branch_name $BRANCH)" >> $REPREPRO_PATH/conf/distributions  || push_error
echo "Label: pantheon-debian-$(get_branch_name $BRANCH)" >> $REPREPRO_PATH/conf/distributions || push_error
echo "Suite: $(get_branch_name $BRANCH)" >> $REPREPRO_PATH/conf/distributions  || push_error
echo "Codename: $(get_branch_name $BRANCH)" >> $REPREPRO_PATH/conf/distributions || push_error
echo "Architectures: source $(get_branch_architectures $BRANCH)" >> $REPREPRO_PATH/conf/distributions || push_error
echo "Components: main contrib" >> $REPREPRO_PATH/conf/distributions || push_error
echo "UDebComponents: main" >> $REPREPRO_PATH/conf/distributions || push_error
echo "SignWith: $(get_pantheon_debian_gpg_key)" >> $REPREPRO_PATH/conf/distributions || push_error
echo "DscIndices: Sources Release . .gz" >> $REPREPRO_PATH/conf/distributions || push_error
echo "" >> $REPREPRO_PATH/conf/distributions || push_error
check_error

trap_error
echo_msgn "CREATE REPREPRO" "Create reprepro $BRANCH ... " $green
reprepro -b $REPREPRO_PATH export  || push_error
check_error

unload_conf
exit 0
